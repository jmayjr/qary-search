# wiki_data.py

import logging
import wikipedia as wiki

from utils import create_uuid

logging.basicConfig(filename='data/wiki_data.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d,%H:%M:%S',
                    level=logging.INFO)

logging.info("Running wiki_data")
logger = logging.getLogger('wiki_data')


def get_data(title):
    """ get wiki page from title and return data """

    try:
        page = wiki.page(title, auto_suggest=False)
    except wiki.exceptions.DisambiguationError:
        logger.info(f"title: {title} DisambiguationError")
        return False
    except wiki.exceptions.PageError:
        logger.info(f"title: {title} PageError")
        return False

    uuid = create_uuid(title)
    tags = ["good articles"]

    data = {
        'uuid': uuid,
        "tags": tags,
        'title': title
    }

    attributes = ['summary', 'categories', 'original_title', 'references',
                  'sections', 'url', 'images', 'pageid', 'revision_id',
                  'links', 'parent_id', 'content']

    for attr in attributes:
        try:
            if getattr(page, attr):
                data[attr] = getattr(page, attr)
        except KeyError:
            pass

    return data
