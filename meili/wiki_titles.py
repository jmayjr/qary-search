# wiki_data.py

import logging
import pandas as pd


logging.basicConfig(filename='data/wiki_data.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d,%H:%M:%S',
                    level=logging.INFO)

logging.info("Running wiki_titles")
logger = logging.getLogger('wiki_titles')

df = pd.read_csv('https://dumps.wikimedia.org/enwiki/latest/enwiki-latest-all-titles-in-ns0.gz', on_bad_lines='skip')

logger.info(f"Number of rows: {len(df)}")

df.to_csv('data/wiki_titles.csv', index=False)
