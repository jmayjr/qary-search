# upload_data.py

import json
import os
import requests


def upload_json(list_of_dicts, index_name='file', primary_key='id'):
    """ upload json data to our meilisearch instance """

    # # process data
    # data is a dict or a list of dicts
    if isinstance(list_of_dicts, (dict, list)):
        data = json.dumps(list_of_dicts)
    # list_of_dicts is a string
    elif isinstance(list_of_dicts, str):
        try:
            # list_of_dicts is a path to file
            with open(list_of_dicts[:256], 'rb') as fin:
                data = fin.read()
        except OSError:
            # is json string
            data = list_of_dicts
    else:
        # data is read from file when ran from command line
        data = list_of_dicts

    url = f'https://docker-meilisearch.onrender.com/indexes/{index_name}/documents?primaryKey={primary_key}'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': os.environ['MEILI_API_KEY']
        # 'Authorization': 'Bearer 56e3023db065fc703db9--JUST AN EXAMPLE--f508d0a21262922c2b12bc9a4f78b06'
    }

    response = requests.post(url, headers=headers, data=data)
    print(response.text)


if __name__ == '__main__':
    with open('file.json', 'rb') as f:
        data = f.read()
    # call function
    upload_json(data)

# make func work  for a list of dicts and a json string that contains a list of dicts - check type
