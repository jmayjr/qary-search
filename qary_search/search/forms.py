from django import forms


class SearchForm(forms.Form):
    input_string = forms.CharField(label='Input String',
                                   widget=forms.TextInput(attrs={'autofocus': True}))
