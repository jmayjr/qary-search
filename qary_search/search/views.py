import json

from django.shortcuts import render

from .forms import SearchForm
from .meili_search import search_meili


def search(request):
    input_string = None
    results = None
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            input_string = form.cleaned_data['input_string']

            query = search_meili(input_string)

            if query:
                j_query = json.loads(query.text)
                hits = j_query.get('hits')

                titles = []
                for hit in hits:
                    titles.append(hit.get('title'))

                if titles:
                    results = titles
                else:
                    results = ['No results found']
            else:
                results = ['could not reach mieli']

    form = SearchForm()
    context = {
        'form': form,
        'results': results,
        'input_string': input_string,
    }

    return render(request, 'search/search.html', context)


def keys(request):
    return render(request, 'search/keys.html')
