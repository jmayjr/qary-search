# meili_search.py

import requests

from django.conf import settings as conf_settings

meili_api_key = conf_settings.MEILI_API_KEY


def search_meili(what_to_search, index_name='wiki', limit=10):
    """ send POST requst to our meilisearch instance """

    url = f'https://docker-meilisearch.onrender.com/indexes/{index_name}/search'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': meili_api_key
    }

    payload = {
        'q': what_to_search,
        'limit': limit,
    }

    response = requests.post(url, headers=headers, json=payload)

    if response.status_code != 200:
        print(response.status_code)
        return None

    return response
